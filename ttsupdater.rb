require 'rubygems'
require 'bundler/setup'
require 'watir'
require 'chronic'
require 'stamp'
require 'pry'
require 'active_support/core_ext'
require 'watir-webdriver'

def holiday?(date)
  holidays = [] #(Date.parse('07-May-2015')..Date.parse('17-May-2015')).to_a
  return true if date.saturday? || date.sunday?
  return true if holidays.include? date
end
def wait(browser,sleep_duration = 0.25)
  p "browser state #{browser.ready_state}"
  p "browser status #{browser.status}"
  sleep sleep_duration
  while (browser.status != "" && browser.ready_state != 'complete' )
    sleep 1
  end
end
def get_form(browser,today)
  browser.text_field(:id => 'ctl00_ContentPlaceHolder1_txtDate').click
  browser.div(:title => today.stamp('Wednesday, April 01, 2015')).click
  wait(browser,sleep_duration = 1)
end
def fill_details(browser)
  p ' no entries for this day. making tts entry'
  browser.select_list(:id => 'ctl00_ContentPlaceHolder1_gvTimesheet_ctl03_ddlNewProjectcode').select 'TSR-090-MED'
  browser.select_list(:id => 'ctl00_ContentPlaceHolder1_gvTimesheet_ctl03_ddlNewActivity').select 'Coding'
  browser.text_field(:id => 'ctl00_ContentPlaceHolder1_gvTimesheet_ctl03_txtNewTask').set '---'
  browser.text_field(:id => 'ctl00_ContentPlaceHolder1_gvTimesheet_ctl03_txtNewDuration').set '8'
  wait(browser)
end
def update(browser)
  browser.button(:name => 'ctl00$ContentPlaceHolder1$gvTimesheet$ctl03$imbInsert').click
end
def login(browser)
  browser.goto("http://tts.experionglobal.com")
  browser.text_field(:name => 'ctl00$ContentPlaceHolder1$txtLoginName').set ARGV[0]
  browser.text_field(:name => 'ctl00$ContentPlaceHolder1$txtPassword').set ARGV[1]
  browser.button(:name => 'ctl00$ContentPlaceHolder1$btnLogin').click
  wait(browser)

end
def browser
  @browser
end
@browser = Watir::Browser.new
login(browser)
start_date = Date.today.at_beginning_of_month
end_date = Date.today
(start_date..end_date).to_a.each do |today|
  begin
    p today
    next if holiday?(today)
    p 'not holiday'
    get_form(browser,today)
    next if browser.span(:id => 'ctl00_ContentPlaceHolder1_gvTimesheet_ctl02_lblTask').present?
    fill_details(browser)
    update(browser)
  rescue => e
    p e
    p 'retry'
    retry
  end
end
